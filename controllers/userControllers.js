//import bcrypt module
const bcrypt = require ('bcrypt');

//Import the User Model
const User = require("../models/User");

const Plane = require("../models/Plane")

//Import auth module
const auth = require ("../auth");
const { get } = require('../routes/planeRoutes');



//CONTROLLERS

//user Registration
module.exports.registerUser = (req, res) => {


    console.log(req.body);


    User.findOne({email: req.body.email})
    .then(result => {
        if(result !== null && result.email === req.body.email)
        {
            return res.send('Email Address is already been Taken, please use another Email')
        }
        else
        {
            const hashedPW = bcrypt.hashSync(req.body.password, 10)


            let newUser = new User ({
                firstName : req.body.firstName,
                lastName : req.body. lastName,
                age: req.body. age,
                address : {
                    city: req.body.city,
                    barangay: req.body.barangay,
                    street: req.body.street
                },
                email : req.body.email,
                mobileNo : req.body.mobileNo,
                password : hashedPW
            })

            if(newUser.age > 18){

                newUser.save()
                .then(user => res.send(user))
                .catch(err => res.send(err));
            
            }
            else{
                    return res.send("Sorry you are underAge to book a Flight")
            }
        }
    })

    .catch(error => res.send(error))

};

//Login User
module.exports.loginUser = (req, res) => {

    console.log(req.body);

        User.findOne({email: req.body.email})
        .then(foundUser => {
            if(foundUser === null){
                return res.send("user does not exist");
            }
            else{
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

                if(isPasswordCorrect){
                    return res.send({accessToken : auth.createAccessToken(foundUser)})
                }
                else{
                    return res.send("Password is incorrect")
                }
            }
        })
        .catch(err => res.send(err));
};


//RETRIEVE ALL USERS 
module.exports.getAllUsers = (req,res) =>{

    User.find({})
    .then(result => {

        if(result.length === 0){
           
            return res.send("No Users")
        }
        else{
            res.send(result)
        }
     }) .catch(err => res.send(err));
}

//RETRIEVE ALL Active USERS 
module.exports.getAllActiveUsers = (req,res) =>{

    User.find({isActive : true})
    .then(result => {

        if(result.length === 0){
           
            return res.send("No Active Users")
        }
        else{
            res.send(result)
        }
    }).catch(err => res.send(err))
   
}

//RETRIEVE ALL InActive USERS 
module.exports.getAllInactiveUsers = (req,res) =>{

    User.find({isActive : false})
    .then(result => {

        if(result.length === 0){
           
            return res.send("No Inactive Users")
        }
        else{
            res.send(result)
        }
    }).catch(err => res.send(err))
}


//get users booking
module.exports.getUserBookings = (req, res) => {
    console.log(req.user.id)
    User.findById(req.user.id)
    .then(user =>{
        if(user.length ===0){
            res.send("No Bookings")
        }
        else
        {
            res.send(user.bookings)
        }
    })
    .catch(err => res.send(err))
};


//get users Details
module.exports.getUserDetails = (req, res) => {
    console.log(req.user.id)
    User.findById(req.user.id)
    .then(user => res.send(user))
    .catch(err => res.send(err))
};

//Update User Details
module.exports.updateUserDetails = (req, res) => {

    console.log(req.user.id);

    const hashedPW = bcrypt.hashSync(req.body.password, 10)

    let updateUser = {

        firstName : req.body.firstName,
        lastName : req.body. lastName,
        age: req.body. age,
        address : {
            city: req.body.city,
            barangay: req.body.barangay,
            street: req.body.street
        },
        mobileNo : req.body.mobileNo,
        password : hashedPW
    }

    User.findByIdAndUpdate(req.user.id, updateUser, {new: true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err));
};

//Cancel A BOOKING
module.exports.cancelBooking = (req, res) =>{

    // console.log(req.params.id)
    // console.log(req.user.id)


    // User.findByIdAndUpdate (req.user.id,
    // {   
    //     "$set" : {
    //         "user.$[elemX].status" : "CANCELLED"
    //     }

    // },
    // {
    //     "arrayFilters" : [{
    //        " elemX.status" : "Upcoming Flight"
    //     }]
    // }
    // )
    // .then (result => res.send(result))
    // .catch (err => res.send(err))

    
    console.log(req.params.id)
    console.log(req.user.id)

    User.findById(req.user.id)
    .then(result =>{

        console.log(result.bookings)

        //getting the specific ID 
        let getbookingID = result.bookings.filter(function(getbooked){
            return (getbooked.id === req.params.id)
        })

        //getting the status EDITED
        let statusresult = getbookingID.filter(function(statusresult){

            if(statusresult.status == "Upcoming Flight")
            {
                return(statusresult.status = "CANCELLED")
            }
            else{
                console.log("Did Nothing")
            }

        })

    User.findByIdAndUpdate(req.params.id, statusresult ,{new:true})
    .then(result => res.send(result))

    
    })
    .catch(err => res.send(err));
};



