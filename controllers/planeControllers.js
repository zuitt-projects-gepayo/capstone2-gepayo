
//Import the User Model
const Plane = require("../models/Plane");

//Import auth module
const auth = require ("../auth");


//CONTROLLERS

//add Plane
module.exports.addPlane = (req, res) => {

    console.log(req.body);

    Plane.findOne({codeName: req.body.codeName})
    .then(result => {
        if(result !== null && result.codeName === req.body.codeName)
        {
            return res.send('Plane Already Added. Please register Another Plane')
        }
        else
        {

            let newPlane = new Plane ({
                codeName : req.body.codeName,
                brand : req.body. brand,
                capacity: req.body. capacity,
                destination: req.body.destination,
                departure: req.body.departure,
                fair: req.body.fair,
            })

                newPlane.save()
                .then(result => res.send(result))
                .catch(err => res.send(err));
        }
    })
    .catch(error => res.send(error))
};

//View all PLanes
module.exports.getAllPlanes = (req,res) =>{

    Plane.find({})
    .then(plane => {
        if(plane.length === 0 )
        {
            res.send("NO AVAILABLE PLANES")
        }
        else{
            res.send(plane)
        }
    })
    .catch(err => res.send(err));
}

//Update Plane departure
module.exports.updatePlaneDeparture =(req, res) => {

    console.log(req.params.id);
    console.log(req.body);

    let updateDeparture = {
        departure : req.body.departure,
        destination : req.body.destination
    }

    Plane.findByIdAndUpdate(req.params.id, updateDeparture, {new: true})
    .then(updatedPlaneDeparture => res.send(updatedPlaneDeparture))
    .catch(err => res.send(err));
};

//View all Active PLanes FOR ADMINS
module.exports.getAllActivePlanes = (req,res) =>{

    Plane.find({planeIsActive : true})
    .then(plane => {
        if(plane.length === 0 )
        {
            res.send("NO ACTIVE PLANES")
        }
        else{
            res.send(plane)
        }
    })
    .catch(err => res.send(err));
}

//View all InActive PLanes FOR ADMINS
module.exports.getAllInactivePlanes = (req,res) =>{

    Plane.find({planeIsActive : false})
    .then(plane => {
        if(plane.length === 0 )
        {
            res.send("NO INACTIVE PLANES")
        }
        else{
            res.send(plane)
        }
    })
    .catch(err => res.send(err));
}


//View all Active PLanes FOR REGULAR USERS
module.exports.getAvailablePlanes = (req,res) =>{

    Plane.find({planeIsActive : true}, {"passengers":0 })
    .then(plane => {
        if(plane.length === 0 )
        {
            res.send("NO AVAILABLE PLANES")
        }
        else{
            res.send(plane)
        }
    })
    .catch(err => res.send(err));
}


//Archive a Plane (ADMIN)
module.exports.archivePlane = (req,res) => {

    console.log(req.params.id);

    let updates = {
        planeIsActive : false
    }

    Plane.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedPlane => res.send(updatedPlane))
    .catch(err => res.send(err));
};

//Activate a PLANE (ADMIN)
module.exports.activatePlane = (req,res) => {

    console.log(req.params.id);

    let updates = {
        planeIsActive : true
    }

    Plane.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then(updatedPlane => res.send(updatedPlane))
    .catch(err => res.send(err));
};
