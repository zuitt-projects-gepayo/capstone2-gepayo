
const Plane = require("../models/Plane");

const User = require("../models/User");

const Booking = require("../models/Booking");

//Import auth module
const auth = require ("../auth");

//CONTROLLERS


//add Booking Flight
module.exports.bookFlight = async (req,res) =>{

    console.log(req.user.id);
    console.log(req.body.planeId);

    if(req.user.isAdmin){
        return res.send("Action Forbidden");
    }

    let isUserBooked = await User.findById(req.user.id)
    .then(user => {

        console.log(user)

        let newBooking = {
            planeId: req.body.planeId
        }

        user.bookings.push(newBooking);

        return user.save()
        .then(user => true)
        .catch(err => err.message);
    })


    if(isUserBooked !== true){

        return res.send({message: isUserBooked})
    }

    let isPlaneBooked = await Plane.findById(req.body.planeId)
    .then(plane => {
        
        console.log(plane);

        let newPassenger = {
            userId : req.user.id
        }

        plane.passengers.push(newPassenger);

        if(plane.capacity )

        return plane.save()
        .then (plane => true)
        .catch(err => err.message);
    })

    if(isPlaneBooked !== true){

        return res.send({message: isPlaneBooked})
    }

    if(isUserBooked && isPlaneBooked ){
        
        return res.send({message: "Booked Successfully"})
    }
};


