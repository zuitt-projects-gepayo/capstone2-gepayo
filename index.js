const express = require('express'); // for the server
const mongoose = require('mongoose'); // for the database


const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const planeRoutes = require('./routes/planeRoutes');
const bookingRoutes = require('./routes/bookingRoutes');

const port = 4000;

const app = express();

mongoose.connect("mongodb+srv://admin_gepayo:admin169@gepayo-169.td1lc.mongodb.net/capstone2-gepayo?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));


//middlewares
app.use(express.json())
app.use(cors());

//use our routes and group together under '/users'
app.use('/users', userRoutes);
app.use('/planes', planeRoutes);
app.use('/bookings', bookingRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))