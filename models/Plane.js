const mongoose = require ('mongoose');

let planeSchema = new mongoose.Schema({

    codeName : {
        type : String,
        required : [true, "Plane Name is Required"]
    },

    brand : {
        type : String,
        required : [true, "Plane Brand is Required"]
    },

    capacity : {
        type : Number,
        required : [true, "Plane Capacity is Required"]
    },

    destination: {
        type : String,
        required : [true, "Plane Routes is Required"]
    },

    departure : {
        type : Date,
        required :[ new Date(), "Date is Required"]
    },

    fair : {
        type: Number,
        required : [true, "Plane fair is Required"]
    },

    planeIsActive : {
        type: Boolean,
        default : true
    },
    passengers : 
    [
        {
            userId : {
                type : String,
                required : [true, "User ID is Required"]
            },

            dateBooked :{
                type : Date,
                default : new Date()
            },

            status :{
                type : String,
                default : "Upcoming Flight"
            }

        }
    ]

});

module.exports = mongoose.model("Plane",planeSchema);