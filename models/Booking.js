const mongoose = require ('mongoose');

let bookingSchema = new mongoose.Schema({

    UserId : {
        type : String,
        required : [true, "User ID is Required"]
    },

    planeId : {
        type : String,
        required : [true, "Plane ID is Required"]
    },

    reservationDate : {
        type : Date,
        default : new Date()
    },
    isActive : {
        type : Boolean,
        default: true
    }

});

module.exports = mongoose.model("Booking",bookingSchema);