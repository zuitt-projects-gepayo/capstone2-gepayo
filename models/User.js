const mongoose = require ('mongoose');

let userSchema = new mongoose.Schema({

    firstName : {
        type : String,
        requred: [true, "First Name is required"]
    },

    lastName :  {
        type: String,
        requred: [true, "Last Name is required"]
    },
    
    age : {
        type: Number,
        required : [true, "Age is required"]
    },

    address : {
        city : {
            type : String,
            requred: [true, "City is required"]
        },
        barangay : {
            type : String,
            requred: [true, "Barangay is required"]
        },
        street : {
            type : String,
            requred: [true, "Street is required"]
        }
    },

    email : {
        type : String,
        requred: [true, "Email is required"]
    },

    password : {
        type : String,
        required : [true, "Password is required"]
    },

    mobileNo : {
        type : String,
        required : [true, "Mobile Number is required"]
    },

    isAdmin : {
        type : Boolean,
        default : false
    },

    isActive : {
        type : Boolean,
        default : true
    },

    bookings : 
    [
        {
            planeId : {
                type : String,
                required : [true, "Plane ID is required"]
            },

            dateBooked :{
                type : Date,
                default : new Date()
            },

            status :{
                type : String,
                default : "Upcoming Flight"
            }
        }
    ]

})

module.exports = mongoose.model("User",userSchema);