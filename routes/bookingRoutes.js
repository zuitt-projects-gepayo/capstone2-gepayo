const express = require ('express');
const router = express.Router();


//import user controller
const bookingControllers = require("../controllers/bookingController");

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

//book a flight
router.post("/bookFlight", verify, bookingControllers.bookFlight);


module.exports = router;

