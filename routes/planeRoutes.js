const express = require ('express');
const router = express.Router();


//import user controller
const planeControllers = require("../controllers/planeControllers");

const auth = require("../auth")

const {verify, verifyAdmin} = auth;


//ADD plane
router.post("/addPlane" ,verify, verifyAdmin, planeControllers.addPlane);

//Get all planes
router.get("/getAllPlanes", verify, verifyAdmin, planeControllers.getAllPlanes);

//Update Plane Departure
router.put("/updatePlaneDeparture/:id", verify, verifyAdmin, planeControllers.updatePlaneDeparture);

//Get all active planes (ADMIN)
router.get("/getAllActivePlanes", verify, planeControllers.getAllActivePlanes);

//Get all Inactive planes (ADMIN)
router.get("/getAllInactivePlanes", verify, planeControllers.getAllInactivePlanes);

//Get all Availabe Planes(FOR REGULAR USERS)
router.get("/getAllAvailablePlanes", verify, planeControllers.getAvailablePlanes);

//archive a Plane (ADMIN)
router.put("/archive/:id", verify, verifyAdmin, planeControllers.archivePlane)

//active a Plane (ADMIN)
router.put("/activate/:id", verify, verifyAdmin, planeControllers.activatePlane)

module.exports = router;