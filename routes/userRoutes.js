const express = require ('express');
const router = express.Router();


//import user controller
const userControllers = require("../controllers/userControllers");

const auth = require("../auth")

const {verify, verifyAdmin} = auth;


//User Registration
router.post("/registration" , userControllers.registerUser);

//Login route
router.post("/login", userControllers.loginUser);

//Get all users
router.get ("/getAllUsers", verify, verifyAdmin, userControllers.getAllUsers)

//Get all active users
router.get ("/getAllActiveUsers", verify, verifyAdmin, userControllers.getAllActiveUsers)

//Get all Inactive users
router.get ("/getAllInactiveUsers", verify, verifyAdmin, userControllers.getAllInactiveUsers)

//Get all users bookings
router.get ("/getUserBookings", verify, userControllers.getUserBookings)

//Get all users details
router.get ("/getUserDetails", verify, userControllers.getUserDetails);

//Update User Details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

router.put ("/cancelBooking/:id", verify, userControllers.cancelBooking);

module.exports = router;