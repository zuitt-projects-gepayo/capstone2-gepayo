//import jsonwebtoken
const jwt = require ('jsonwebtoken');
const secret = "FlightBookingAPI";

/*
    NOTES:
        JWT is a way to securely pass information from one part of a server to the frontend or the other parts of our application. this will allow us to authorize our users to access or disallow access to certain parts of our app.

        JWT is like a gift wrapping service that is able to encode our user details can only be unwrapped by JWT's own methods and if the secret provided is intact.

        IF the JWT seemed tampered with, we will reject the users attempt to access a feature

*/

module.exports.createAccessToken= (user) => {

    const data ={
        id : user._id,
        email : user.email,
        isAdmin : user.isAdmin
    }

    return jwt.sign(data, secret, {})
};

/*
    NOTES:
        >> you can only get a unique JWT with our secret if you log in to our  app with the correct email and password
        >> as  a user, you can only get  your own details from your own token from loggin in
        >>JWT is not meant to store sensitive data. For now for ease of use and for our MVP (minimum viable Product). we add the email and isAdmin details of the logged in user. However, in the future, you can limit this to only ID and for every route and feature, you can simply look up for the user in the database to get his details

        >> we will verify the legitamacy of a JWT every time a user access a restricted feature. Each JWT contains a secret only our server knows. IF the jwt has been changed in any way, we will reject the user and his tampered token. IF the JWT does not contain a secret OR tge secret is different, we will reject his access and token
*/

module.exports.verify = (req, res, next) => {
    
    //req.headers.authorization contains sensitive data and specially our token/JWT

    let token = req.headers.authorization

    console.log(token);

    if(typeof token === "undefined"){
        return res.send({auth: "Failed. No Token"})
    }
    else{

        token = token.slice(7, token.length)

        jwt.verify(token, secret, (err, decodedToken)=> {
            if(err){
                return res.send({
                    auth : "Failed",
                    message : err.message
                });
            }
            else {
                req.user = decodedToken;

                next();
            }
        })

    }
};

module.exports.verifyAdmin = (req, res, next) => {

    if(req.user.isAdmin){
        next();
    }
    else{
        return res.send ({
            auth : "Failed",
            message : "You Dont have access! Authorized Person only"
        })
    }
};